package exec2;

import java.util.Scanner;
import java.util.Stack;

public class Program {
	static Scanner sc = new Scanner(System.in);
	
	//Kaiak
	//Davi
	public static void main(String[] args) {		
		System.out.print("Informe a palavra: ");
		String palavra = sc.nextLine();
		palavra = palavra.replaceAll("\\p{Punct}", "");
		palavra = palavra.trim().replaceAll("\\s","");
		String newPalavra = reescrevePalavra(palavra);
		if (palavra.equals(newPalavra)) {
			System.out.println("� um Pal�ndromo");
		}else {
			System.out.println("N�o � um Pal�ndromo");
		}

	}

	private static String reescrevePalavra(String palavra) {
		Stack<Character> letras = new Stack<>();
		for (char letra : palavra.toCharArray()) {
			letras.push(letra);
		}
		
		StringBuilder sb = new StringBuilder();
		while(letras.size() != 0) {
			sb.append(letras.pop());
		}
		
		return sb.toString();
	}

}
