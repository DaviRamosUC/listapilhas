package exec5;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Program {

	public static int posicaoPilha = 0;
	public static List<Object> pilha;
	private static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		do {
			System.out.print(
					"\n1)Inicializar pilha \n2)Adicionar elemento \n3)Remover \n4)Sair \nInforme o que deseja fazer: ");
			int op = sc.nextInt();
			sc.nextLine();
			switch (op) {
			case 1: {
				System.out.println("Pilha inicializada!");
				inicializarPilha();
				break;
			}
			case 2: {
				System.out.println("O que deseja por na pilha?");
				Object a = sc.nextLine();
				push(pilha, a);
				break;
			}
			case 3: {
				pop(pilha);
				break;
			}
			case 4: {
				System.out.println("At� mais ver :P");
				System.exit(0);
				break;
			}
			default:
				System.out.println("Op��o n�o cadastrada!");
			}
			if (!pilha.isEmpty()) {
				for (Object obj : pilha) {
					System.out.println(obj);
				}
			}

		} while (true);

	}

	public static void inicializarPilha() {
		pilha = new ArrayList<Object>();
	}

	public static void push(List<Object> list, Object obj) {
		list.add(obj);
	}
	
	public static String pilhaVazia(List<Object> list) {
		return (pilha.isEmpty()) ? "Todas as tarefas foram conclu�das": "Ainda h� tarefas";
	}

	public static Object pop(List<Object> list) {
		int posicao= list.size()-1;
		Object ret = list.get(posicao);
		list.remove(posicao);
		return ret;
	}

}
