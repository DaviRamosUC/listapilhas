package exec1;

import java.util.Iterator;
import java.util.Scanner;
import java.util.Stack;

public class Program {

	static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		Stack<Livro> livros = new Stack<>();
		int op = 0;

		do {
			System.out.print(
					"Informe qual opera��o deseja fazer: \n1) Inserir Livro \n2)Consultar Livro \n3)Remover \n4)Esvaziar \n5)Sair");
			op = sc.nextInt();
			sc.nextLine();
			switch (op) {
			case 1: {
				livros.push(construirLivro());
				break;
			}
			case 2: {
				System.out.print("Informe o nome do livro que deseja consultar: ");
				String nome = sc.nextLine(); 
				for (Livro livro : livros) {
					if (livro.getNome().equals(nome)) {
						System.out.println(livro);
					}
				}
				break;
			}
			case 3: {
				System.out.print("Informe o nome do livro que deseja remover: ");
				String nome = sc.nextLine(); 
				Iterator<Livro> value = livros.iterator();
				while (value.hasNext()) {
					Livro livro = value.next();
					value.remove();
					if (livro.getNome().equals(nome)) {
						livros.remove(livro);
					}
				}
				break;
			}
			case 4: {
				livros.clear();
				System.out.println("Sua lista tem: "+ livros.size());
				break;
			}
			default:
				System.out.println("Op��o n�o registrada!");
			}

		} while (op != 5);

	}

	public static Livro construirLivro() {
		System.out.print("Informe o nome do livro: ");
		String nome = sc.nextLine();
		return new Livro(nome);
	}

}
