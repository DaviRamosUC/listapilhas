package exec1;

public class Livro {
	
	private String nome;

	public Livro() {
		super();
	}

	public Livro(String nome) {
		super();
		this.nome = nome;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	@Override
	public String toString() {
		return "Livro: " + nome;
	}
	
	
}
